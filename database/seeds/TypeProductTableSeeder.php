<?php

use Illuminate\Database\Seeder;

class TypeProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('type_products')->insert([
        	['name' => 'Bánh Sinh Nhật', 'description' => 'Bánh sinh nhật cho mọi lứa tuổi.', 'image' => '210215-banh-sinh-nhat-rau-cau-body.jpg'],
        	['name' => 'Bánh Kem', 'description' => 'Bánh kem đậm hương vị thiên nhiên.', 'image' => 'banhkem.jpg'],
        	['name' => 'Bánh Su', 'description' => 'Bánh su thơm ngất ngây.', 'image' => '1434429117-banh-su-kem-chien-20.jpg'],
        	['name' => 'Bánh Trái Cây', 'description' => 'Bánh trái cây được làm từ những trái cây nguyên chất, an toàn.', 'image' => 'Fruit-Cake_7976.jpg'],
        ]);
    }
}