<?php

use Illuminate\Database\Seeder;

class SlideTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('slide')->insert([
        	['link' => 'link 1', 'image' => 'banner1.jpg'],
        	['link' => 'link 2', 'image' => 'banner2.jpg'],
        	['link' => 'link 3', 'image' => 'banner3.jpg'],
        	['link' => 'link 4', 'image' => 'banner4.jpg'],
        ]);
    }
}
