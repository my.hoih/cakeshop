<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
        	[
        		'name' => 'Bánh Sinh Nhật Rau Cau',
        		'id_type' => '1',
        		'description' => 'Bánh sinh nhật rau cau từ thiên nhiên',
        		'unit_price' => '150000',
        		'promotion_price' => '0',
        		'image' => 'banh kem sinh nhat.jpg',
        		'unit' => 'cái',
        		'new' => '1'
        	],
        	[
        		'name' => 'Bánh Sinh Nhật Lạ',
        		'id_type' => '1',
        		'description' => 'Bánh sinh nhật rau cau vừa độc vừa lạ',
        		'unit_price' => '190000',
        		'promotion_price' => '165000',
        		'image' => '210215-banh-sinh-nhat-rau-cau-body.jpg',
        		'unit' => 'cái',
        		'new' => '1'
        	],
        	[
        		'name' => 'Bánh Kem',
        		'id_type' => '2',
        		'description' => 'Bánh kem ngon từ thiên nhiên',
        		'unit_price' => '110000',
        		'promotion_price' => '0',
        		'image' => 'Banh-kem (6).jpg',
        		'unit' => 'cái',
        		'new' => '1'
        	],
    		[
	    		'name' => 'Bánh Su Que',
	    		'id_type' => '3',
	    		'description' => 'Bánh su que phô mai',
	    		'unit_price' => '120000',
	    		'promotion_price' => '100000',
	    		'image' => '50020041-combo-20-banh-su-que-pho-mai-9.jpg',
	    		'unit' => 'cái',
	    		'new' => '1'
        	],
        	[
                'name' => 'Bánh Trái Cây',
                'id_type' => '4',
                'description' => 'Bánh trái cây hòa trộn nhiều hương vị thiên nhiên',
                'unit_price' => '170000',
                'promotion_price' => '145000',
                'image' => 'Fruit-Cake_7971.jpg',
                'unit' => 'hộp',
                'new' => '1'
            ],
            [
                'name' => 'Bánh Kem Dâu',
                'id_type' => '2',
                'description' => 'Bánh kem dâu hòa trộn nhiều hương vị thiên nhiên nhất là hương vị thơm, ngọt của dâu, bạn sẽ cảm thấy rất mát mẻ.',
                'unit_price' => '150000',
                'promotion_price' => '0',
                'image' => 'banhkem-dau.jpg',
                'unit' => 'cái',
                'new' => '1'
            ],
            [
                'name' => 'Bánh Su Kem Dâu',
                'id_type' => '3',
                'description' => 'Bánh sukem dâu với lớp nhân béo béo kèm với vị chua dịu của miếng dâu tây làm cho miếng bánh tăng thêm vị ngọt hơn.',
                'unit_price' => '119000',
                'promotion_price' => '0',
                'image' => 'sukemdau.jpg',
                'unit' => 'hộp',
                'new' => '1'
            ],
            [
	    		'name' => 'Bánh Trái Cây Trộn',
	    		'id_type' => '4',
	    		'description' => 'Giải nhiệt ngày hè không gì tuyệt bằng trái cây trộn. Bạn hãy ghi nhớ 2 cách làm trái cây trộn kinh điển này nhé!',
	    		'unit_price' => '90000',
	    		'promotion_price' => '0',
	    		'image' => 'banhtraicay.jpg',
	    		'unit' => 'hộp',
	    		'new' => '1'
        	],
            [
                'name' => 'Bánh Sinh Nhật',
                'id_type' => '1',
                'description' => 'Bánh sinh nhật đã gắn liền và không thể nào thiếu được trong những buổi tiệc sinh nhật . Nó như một thủ tục , nguyên tắc quan trọng ngầm định mà bất kỳ một bữa tiệc tổ chức sinh nhật nào cũng được nhắc tới và thực sự không thể nào thiếu được. Chiếc bánh như một biểu tượng thiêng liêng đầy ý nghĩa . Nhưng liệu bạn đã bao giờ tự đặt cho mình những câu hỏi tò mò về chiếc bánh vốn dĩ đã quá đỗi quen thuộc trong cuộc sống hàng ngày của mỗi chúng ta.',
                'unit_price' => '190000',
                'promotion_price' => '160000',
                'image' => 'banhkem.jpg',
                'unit' => 'cái',
                'new' => '0'
            ],
            [
                'name' => 'Bánh Su Kem',
                'id_type' => '2',
                'description' => 'Bánh su kem, một món tráng miệng đơn giản mà cực ngon. Lớp vỏ ngoài thơm mùi trứng, nhân custard bên trong thì béo ngậy thơm mùi sữa và vani, chắc hẳn các em nhỏ đều yêu thích và ngay cả người lớn.',
                'unit_price' => '20000',
                'promotion_price' => '0',
                'image' => 'banhkem-dau.jpg',
                'unit' => 'cái',
                'new' => '1'
            ],
            [
                'name' => 'Bánh Su Kem Cà Phê',
                'id_type' => '3',
                'description' => 'Món bánh có phần vỏ thơm, xốp và phần nhân mịn mượt, béo vừa phải, thơm mùi cà phê chắc chắn sẽ khiến người nhận được.',
                'unit_price' => '130000',
                'promotion_price' => '0',
                'image' => 'banh-su-kem-ca-phe-1.jpg',
                'unit' => 'cái',
                'new' => '0'
            ],
            [
                'name' => 'Bánh Trái Cây Kem',
                'id_type' => '4',
                'description' => 'Mùa hè với đa dạng chủng loại trái cây ngon ngọt, hãy thử kết hợp chúng với chiếc bánh gato kem xem sao.',
                'unit_price' => '99000',
                'promotion_price' => '90000',
                'image' => 'Fruit-Cake.jpg',
                'unit' => 'cái',
                'new' => '1'
            ],
        ]);
    }
}
