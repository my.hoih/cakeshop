<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDonhangViews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE VIEW views_don_hang AS
            (
            SELECT b.date_order, b.total, b.payment, b.note, b.id_customer, p.name, c.nameCus, c.address, c.email, c.phone_number, bd.id_product, bd.quantity

            FROM bills as b, products as p, customer as c, bill_detail as bd
            WHERE b.id = bd.id_bill
                AND p.id = bd.id_product
                AND c.id = b.id_customer
            Order By b.id asc
            )
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS views_don_hang');
    }
}
