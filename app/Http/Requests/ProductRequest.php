<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'txtProName'            => 'required',
            'txtIdType'             => 'required',
            'txtProDescription'     => 'required',
            'txtProPrice'           => 'required|numeric',
            'txtProImage'           => 'required|image',
            'txtProUnit'            => 'required',
            'txtProNew'             => 'required|numeric'
        ];
    }

    public function messages() {
        return [
            'txtProName.required'           => 'Vui lòng nhập tên sản phẩm.',
            'txtIdType.required'            => 'Vui lòng chọn id danh mục sản phẩm.',
            'txtProDescription.required'    => 'Vui lòng nhập mô tả cho sản phẩm.',
            'txtProPrice.required'          => 'Vui lòng nhập giá cho sản phẩm.',
            'txtProPrice.numeric'           => 'Giá phải là số.',
            'txtProImage.required'          => 'Vui lòng chọn ảnh cho danh mục sản phẩm.',
            'txtProImage.image'             => 'Định dạng phải là ảnh.',
            'txtProUnit.required'           => 'Vui lòng nhập đơn vị cho sản phẩm.',
            'txtProNew.required'            => 'Vui lòng nhập loại hình cho sản phẩm.',
            'txtProNew.numeric'             => 'Loại hình phải là số.'
        ];
    }
}
