<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'txtCateName'               => 'required',
                'txtCateDescription'        => 'required',
                'txtimage'                  => 'required|image'
        ];
    }

    public function messages() {
        return [
            'txtCateName.required'          => 'Vui lòng nhập tên danh mục sản phẩm.',
            'txtCateDescription.required'   => 'Vui lòng nhập mô tả cho danh mục sản phẩm.',
            'txtimage.required'             => 'Vui lòng chọn ảnh cho danh mục sản phẩm.',
            'txtimage.image'                => 'Định dạng phải là ảnh.'
        ];
    }
}
