<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CategoryRequest;
use App\Http\Requests\ProductRequest;
use Requests;
use App\Slide;
use App\Product;
use App\ProductType;
use App\Cart;
use Session;
use App\Customer;
use App\Bill;
use App\BillDetail;
use App\User;
use Hash;
use Auth;
use Input,File;
use DB;
use App\views_don_hang;

class PageController extends Controller
{
    public function getIndex() {
        $slide              = Slide::all();
        $new_product        = Product::where('new','1')->paginate(4);
        $sanpham_khuyenmai  = Product::where('promotion_price','<>','0')->orderby('promotion_price')->paginate(4);
    	return view('page.trangchu', compact('slide', 'new_product', 'sanpham_khuyenmai'));
    }

    public function getLoaiSp($type) {
        $loai_sp         = ProductType::where('id', $type)->first(); //lấy tên danh mục sp vừa clik trên  menu ngang(first(): lấy data khớp với từng id)
        $loai_menuleft   = ProductType::all(); //lấy tên hiển thị trong menu left của loai_sanpham file
        $new_product     = Product::where('new','1')->paginate(4);
        $sp_tuongtu      = Product::where('id_type', $type)->paginate(3);
    	return view('page.loai_sanpham', compact('loai_sp', 'loai_menuleft', 'new_product', 'sp_tuongtu'));
    }

    public function getChitietSp(Request $request) {
        $sanpham        = Product::where('id', $request->id)->first();
        $sp_tuongtu     = Product::where('id_type', $sanpham->id_type)->paginate(3);
        $sp_moi         = Product::where('new', '1')->paginate(4);
    	return view('page.chitiet_sanpham', compact('sanpham', 'sp_tuongtu', 'sp_moi'));
    }

    public function getLienhe() {
    	return view('page.lienhe');
    }

     public function getGioithieu() {
    	return view('page.about');
    }

    public function getCheckout() {
        return view('page.checkout');
    }

    public function getLogin() {
        return view('page.login');
    }

    public function getSignup() {
        return view('page.signup');
    }

    // Hàm đăng ký tài khoản
    public function postSignup(Request $req) {
        $this->validate($req,
            [
                'email'             => 'required|email|unique:users,email',
                'password'          => 'required|min:6|max:20',
                'fullname'          => 'required',
                'gender'            => 'required',
                'address'           => 'required',
                'phone'             => 'required|numeric',
                're_password'       => 'required|same:password'
            ],
            [
                'email.required'    => 'Vui lòng nhập email.',
                'email.eamil'       => 'Không đúng định dạng email.',
                'email.unique'      => 'Email đã có người sử dụng.',
                'password.required' => 'Vui lòng nhập mật khẩu.',
                'fullname.required' => 'Vui lòng nhập họ tên.',
                're_password.same'  => 'Mật khẩu không giống nhau.',
                'password.min'      => 'Mật khẩu ít nhất 6 ký tự.',
                'password.max'      => 'Mật khẩu không quá 20 ký tự.',
                'gender.required'   => 'Vui lòng chọn giới tính.',
                'address.required'  => 'Vui lòng nhập địa chỉ.',
                'phone.required'    => 'Vui lòng nhập số điện thoại.',
                'phone.numeric'     => 'Số điện thoại phải là số.'
            ]);
        // $user                       = new User();
        // $user->name                 = $req->fullname;
        // $user->gender               = $req->gender;
        // $user->address              = $req->address;
        // $user->phone                = $req->phone;
        // $user->email                = $req->email;
        // $user->password             = Hash::make($req->password);
        // $is_saved = $user->save();
        Product::create($request->all());
        if (is_saved){
            $user->id;
        }
        
        return redirect()->back()->with('success','Tạo tài khoản thành công.');
    }

    // Hàm đăng nhập
    public function postLogin(Request $req) {
        $this->validate($req,
            [
                'email'             => 'required|email',
                'password'          => 'required|min:6|max:20'
            ],
            [
                'email.required'    => 'Vui lòng nhập email.',
                'email.email'       => 'Không đúng định dạng email.',
                'password.required' => 'Vui lòng nhập mật khẩu.',
                'password.min'      => 'Mật khẩu ít nhất 6 ký tự.',
                'password.max'      => 'Mật khẩu không quá 20 ký tự.'
            ]);
        $credentials = array('email' => $req->email, 'password' => $req->password);
        if (Auth::attempt($credentials)) {
            return redirect()->back()->with('success','Đăng nhập thành công.');
        }else {
            return redirect()->back()->with('error','Đăng nhập thất bại.');
        }
    }

    // Hàm đăng xuất
    public function postLogout() {
        Auth::logout();
        return redirect()->route('trangchu');
    }

    // Hàm tìm kiếm sản phẩm
    public function getSearch(Request $req) {
        $product = Product::where('name', 'like', '%'.$req->key.'%')
                            ->orwhere('unit_price', $req->key)
                            ->get();
        return view('page.search', compact('product'));
    }

    // Hàm thêm sp vào cart
    public function getAddtoCart(Request $req, $id) {
        $product    = Product::find($id);
        $oldCart    = Session('cart')?Session::get('cart'):null;
        $cart       = new Cart($oldCart);
        $cart->add($product, $id);
        $req->session()->put('cart',$cart);
        return redirect()->back();
    }

    // Hàm xóa 1 sp trong cart
    public function getDelItemCart($id) {
        $oldCart    = Session::has('cart')?Session::get('cart'):null;
        $cart       = new Cart($oldCart);
        $cart->removeItem($id);
        if (count($cart->items) > 0) {
            Session::put('cart',$cart); // Updates a resource.
        }else {
            Session::forget('cart');
        }
        return redirect()->back();
    }

    // Hàm lưu data vào database
    public function postCheckout(Request $req) {
        $cart = Session::get('cart');

        $customer               = new Customer;
        $customer->nameCus      = $req->name;
        $customer->gender       = $req->gender;
        $customer->email        = $req->email;
        $customer->address      = $req->address;
        $customer->phone_number = $req->phone;
        $customer->note         = $req->notes;
        $customer->save();

        $bill                   = new Bill;
        $bill->date_order       = date('Y-m-d');
        $bill->total            = $cart->totalPrice;
        $bill->payment          = $req->payment_method;
        $bill->note             = $req->notes;
        $bill->id_customer      = $customer->id;
        $bill->save();

        foreach ($cart->items as $key => $value) {
            $bill_detail                = new BillDetail;
            $bill_detail->id_bill       = $bill->id;
            $bill_detail->id_product    = $key;
            $bill_detail->quantity      = $value['qty'];
            $bill_detail->unit_price    = ($value['price']/$value['qty']);
            $bill_detail->save();
        }
        Session::forget('cart');
        return redirect()->back()->with('success','Đặt hàng thành công.');
    }

    // Hàm list loại sp
    public function getCategory() {
        $cate       = ProductType::all();
        return view('admin.category', compact('cate'));
    }

    // Thêm loại sp
    public function getAddCate() {
        $addcate    = ProductType::all();
        return view('admin.cate_add', compact('addcate'));
    }

    public function postAddCate(CategoryRequest $request) {
        $category               = new ProductType;
        $category->name         = $request->txtCateName;
        $category->description  = $request->txtCateDescription;
        $file_name              = $request->file('txtimage')->getClientOriginalName();
        $category->image        = $file_name;
        $request->file('txtimage')->move('public/image/product/',$file_name);
        $category->save();
        return redirect()->route('danhmucsp')->with('success','Thêm danh mục sản phẩm thành công.');
    }

    // Sửa loại sp
    public function getEditCate($id) {
        $EditCate = ProductType::find($id);
        return view('admin.cate_edit', compact('EditCate'));
    }

    public function postEditCate($id, CategoryRequest $req) {
        $category               = ProductType::find($id);
        $category->name         = $req->txtCateName;
        $category->description  = $req->txtCateDescription;
        $img_current            = 'public/image/product/'. $req->img_current;
        if(!empty($req->txtimage))
        {
            $file_name          = $req->txtimage->getClientOriginalName();
            $category->image    = $file_name;
            $req->txtimage->move('public/image/product/',$file_name);
            if(File::exists($img_current))
            {
                File::delete($img_current);
            }
        }
        $category->save();
        return redirect()->route('danhmucsp')->with('success','Chỉnh sửa danh mục sản phẩm thành công.');
    }

    // Hàm xóa loại sp
    public function getDelCate($id) {
        $category = ProductType::find($id);
        // Xoa cate and delete product has id this cate
        // $child_product = ProductType::find($id)->type_products();
        // File::delete('public/image/product/'.$category->image);// xóa lun ảnh trong folder chứa ảnh đó
        $category->delete($id);
        // ProductType::find($id)->delete();
        return back()->with('success','Xóa danh mục sản phẩm thành công!');
    }

    // Hàm list sp
    public function getProduct() {
        $product = Product::select('id','name','id_type','description','unit_price','promotion_price','image','unit','new')->paginate(8);
        return view('admin.product-list', compact('product'));
    }

    // Hàm thêm sản phẩm
    public function getAddPro() {
        $product = Product::all();
        $cate    = ProductType::all();
        return view('admin.pro-add', compact('product', 'cate'));
    }

    public function postAddPro(ProductRequest $request) {
        $product                    = new Product;
        $product->name              = $request->txtProName;
        $product->id_type           = $request->txtIdType;
        $product->description       = $request->txtProDescription;
        $product->unit_price        = $request->txtProPrice;
        $product->promotion_price   = $request->txtProPromotionPrice;
        $file_name                  = $request->file('txtProImage')->getClientOriginalName();
        $product->image             = $file_name;
        $request->file('txtProImage')->move('public/image/product/',$file_name);
        $product->unit              = $request->txtProUnit;
        $product->new               = $request->txtProNew;
        $product->save();
        return redirect()->back()->with('success', 'Thêm sản phẩm thành công.');
    }

    // Hàm sửa sản phẩm
    public function getEditPro($id) {
        $cate       = ProductType::all();
        $EditPro    = Product::find($id);
        return view('admin.pro-edit', compact('cate', 'EditPro'));
    }

    public function postEditPro($id, Request $req) {
        $product                    = Product::find($id);
        $product->name              = Request::input('txtProName');
        $product->id_type           = Request::input('txtIdType');
        $product->description       = Request::input('txtProDescription');
        $product->unit_price        = Request::input('txtProPrice');
        $product->promotion_price   = Request::input('txtProPromotionPrice');
        $img_current                = 'public/image/product/'. Request::input('img_current');
        if(!empty(Request::file('txtProImage')))
        {
            $file_name              = Request::file('txtProImage')->getClientOriginalName();
            $product->image         = $file_name;
            Request::file('txtProImage')->move('public/image/product/',$file_name);
            if(File::exists($img_current))
            {
                File::delete($img_current);
            }
        }
        $product->unit              = Request::input('txtProUnit');
        $product->new               = Request::input('txtProNew');
        $product->save();
        return redirect()->back()->with('success','Chỉnh sửa sản phẩm thành công.');
    }

    // Hàm xóa sản phẩm
    public function getDelPro($id) {
        $product = Product::find($id);
        $product->delete($id);
        return redirect()->back()->with('success', 'Xóa sản phẩm thành công.');
    }

    // Hàm danh sách có tài khoản người dùng
    public function getUser() {
        $user = User::all();
        return view('admin.user-list', compact('user'));
    }

    // Hàm danh sách khách hàng
    public function getCustomer() {
        $customer = Customer::all();
        return view('admin.customer-list', compact('customer'));
    }

    // Hàm xóa tk khách hàng
    public function getDelCus($id) {
        $customer = Customer::find($id);
        $customer->delete($id);
        return redirect()->back()->with('success', 'Xóa tài khoản khách hàng thành công.');
    }

    // Hàm danh sách đơn hàng
    public function getBill() {
        $view = views_don_hang::all();
        return view('admin.bill-list', compact('view'));
    }

    // Hàm xóa đơn hàng
    public function getDelBill($id) {
        $bill = Bill::find($id);
        $bill->delete($id);
        return redirect()->back()->with('success', 'Xóa đơn hàng thành công.');
    }
}
