<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use App\Product;
use Session;

class NewsController extends Controller
{
    public function index() {
    	return view('admin.new');
    }

    public function readData() {
        $news = News::select('id', 'title', 'content')->get();
        return view('admin.new-list', compact('news'));
    }

    public function store(Request $request)
    {
        if ($request->ajax())
        {
            $new = News::create($request->all());
            $this->find($new->id);
            return response($this->find($new->id));
        }
    }

    public function find($id) {
        return News::where('id', $id)->first();
    }

    public function destroy(Request $request) {
        if ($request->ajax())
        {
            News::destroy($request->id);
            return response(['id'=>$request->id]);
        }
    }

    public function edit(Request $request) {
        if ($request->ajax())
        {
            $news = News::find($request->id);
            return response($news);
        }
    }

    public function update(Request $request) {
        if ($request->ajax())
        {
            $news = News::find($request->id);
            $news->update($request->all());
            return response($this->find($request->id));
        }
    }

}
