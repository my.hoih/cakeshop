@extends('admin.dashboard')
@section('content')
@include('admin.new-add')
@include('admin.new-edit')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        News
        <small>List</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">News</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Hello admin!</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>
    <!-- /.box -->

    <div class="container-fluid">
    	<div class="row">
            <div class="panel-heading">

                <button class="btn btn-info" data-toggle="modal" data-target="#myModal">+New</button>

                <button class="btn btn-info pull-right" id="read-data">Load Data</button>
            </div>

    		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	            <table class="table table-striped table-bordered table-hover">
	                <thead>
	                    <tr>
	                        <th>ID</th>
	                        <th>Title</th>
                            <th>Content</th>
                            <th>Edit</th>
	                        <th>Delete</th>
	                    </tr>
	                </thead>
	                <tbody id="news-info">
	                	
	                </tbody>
	            </table>
            </div><!-- /.col-lg-12 -->
    	</div>
    </div>
</section>
<!-- /.content -->
@endsection

@section('script')
<script type="text/javascript">

    // ---------------------------------------------------
    $('#read-data').on('click',function()
    {
        $.get("{{ URL::to('admin/read-data') }}",function(data)
        {
            $('#news-info').empty().html(data);
        })
    });

    // -------------- add tin tuc -------------------------
    $('#frm-insert').on('submit',function(e)
    {
        e.preventDefault();
        var data = $(this).serialize();
        console.log(data);
        var url = $(this).attr('action');
        var post = $(this).attr('method');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type : post,
            url : url,
            data : data,
            dataType : 'json',
            success:function(data)
            {
                var tr = $('<tr/>',{
                    id : data.id
                });
                tr.append($("<td/>",{
                    text: data.id
                })).append($("<td/>",{
                    text: data.title
                })).append($("<td/>",{
                    text: data.content
                })).append($("<td/>",{
                    html: '<a href="#" class="btn btn-info" id="view" data-id="'+data.id+'">View</a>'
                })).append($("<td/>",{
                    html: '<a class="btn btn-success" id="edit" data-id="'+data.id+'">Edit</a>'
                })).append($("<td/>",{
                    html: '<a class="btn btn-danger" id="del" data-id="'+data.id+'">Delete</a>'
                }))

                $('#news-info').append(tr);
            }
        })
    });

    // ---------------- delete tin tuc -----------------------
    $('body').delegate('#news-info tr #del','click',function(e){
        var id = $(this).data('id');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type : 'post',
            url  : "{{ url('admin/destroy') }}",
            data : {id:id},
            dataType: 'json',
            success:function(data)
            {
                $('tr#'+id).remove();
            }
        })
    });

    // ---------------- edit tin tuc ------------------------
    $('body').delegate('#news-info #edit','click',function(e){
        var id = $(this).data('id');
        $.get("{{ URL::to('admin/edit') }}",{id:id},function(data)
        {
            $('#frm-update').find('#title').val(data.title)
            $('#frm-update').find('#content').val(data.content)
            $('#frm-update').find('#id').val(data.id)
            $('#news-update').modal('show');
        })
    });

    // ---------------- update tin tuc ----------------------
    $('#frm-update').on('submit',function(e) {
        e.preventDefault();
        var data = $(this).serialize();
        var url = $(this).attr('action');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type : 'post',
            url  : url,
            data : data,
            dataType: 'json',
            success:function(data)
            {
                $('#frm-update').trigger('reset');

                var tr = $('<tr/>',{
                    id : data.id
                });
                tr.append($("<td/>",{
                    text: data.id
                })).append($("<td/>",{
                    text: data.title
                })).append($("<td/>",{
                    text: data.content
                })).append($("<td/>",{
                    html: '<a class="btn btn-success" id="edit" data-id="'+data.id+'">Edit</a>'
                })).append($("<td/>",{
                    html: '<a class="btn btn-danger" id="del" data-id="'+data.id+'">Delete</a>'
                }))

                $('#news-info tr#'+data.id).replaceWith(tr);
            }
        })
    });

</script>
@endsection