@extends('admin.dashboard')
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        User
        <small>List</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">User</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Hello admin!</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>
    <!-- /.box -->

    <div class="container-fluid">
    	<div class="row">
            <!-- Display message -->
            @include('flash-message')
            <!-- //Display message -->
    		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
	                <thead>
	                    <tr>
	                        <th>ID</th>
	                        <th>Tên tài khoản người dùng</th>
	                        <th>Giới tính</th>
                            <th>Địa chỉ</th>
	                        <th>Số điện thoại</th>
	                        <th>Email</th>
	                    </tr>
	                </thead>
	                <tbody>
	                	@foreach($user as $users)
                        <tr class="odd gradeX" align="center">
                            <td>{{ $users->id }}</td>
                            <td>
                                <div class="form-group">
                                    <input class="form-control" name="nameUser" type="text" value="{{ $users->name }}" style="background: transparent; border: none;" readonly>
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <input class="form-control" name="genderUser" type="text" value="{{ $users->gender }}" style="background: transparent; border: none;" readonly>
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <input class="form-control" name="addressUser" type="text" value="{{ $users->address }}" style="background: transparent; border: none;" readonly>
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <input class="form-control" name="phoneUser" type="text" value="{{ $users->phone }}" style="background: transparent; border: none;" readonly>
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <input class="form-control" name="emailUser" type="text" value="{{ $users->email }}" style="background: transparent; border: none;" readonly>
                                </div>
                            </td>
                        </tr>
                        @endforeach
	                </tbody>
	            </table>
            </div><!-- /.col-lg-12 -->
    	</div>
    </div>
</section>
<!-- /.content -->

@endsection