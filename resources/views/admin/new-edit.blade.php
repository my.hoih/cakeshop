<!-- Modal -->
<div id="news-update" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tin tức mới</h4>
            </div>
            <form action="{{ URL::to('admin/update') }}" method="POST" id="frm-update" >
                <div class="modal-body">

                    <div class="col-4-md">
                        <div class="form-group">
                            <label>Title</label>
                            <input type="hidden" class="form-control" name="id" id="id">
                            <input type="text" class="form-control" name="title" id="title">
                        </div>
                    </div>

                    <div class="col-4-md">
                        <div class="form-group">
                            <label>Content</label>
                            <input type="text" class="form-control" name="content" id="content">
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-success pull-left" value="Update">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>