@extends('admin.dashboard')
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Product
        <small>List</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Product</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Hello admin!</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>
    <!-- /.box -->

    <div class="container-fluid">
    	<div class="row">
            <!-- Display message -->
            @include('flash-message')
            <!-- //Display message -->
    		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
	                <thead>
	                    <tr>
	                        <th>ID</th>
	                        <th>Tên sản phẩm</th>
	                        <th>ID danh mục sản phẩm</th>
	                        <th>Mô tả</th>
	                        <th>Giá</th>
	                        <th>Hình ảnh</th>
	                        <th>Đơn vị</th>
	                        <th>Loại hình</th>
	                        <th>Chỉnh sửa</th>
	                        <th>Xóa</th>
	                    </tr>
	                </thead>
	                <tbody>
	                	@foreach($product as $pro)
                        <tr class="odd gradeX" align="center">
                            <td>{{ $pro->id }}</td>
                            <td>
                                <div class="form-group">
                                    <textarea class="form-control" name="nameProduct" type="text" style="background: transparent; border: none;" readonly>{{ $pro->name }}</textarea>
                                </div>
                            </td>

                            <td>{{ $pro->id_type }}</td>

                            <td>
                                <div class="form-group">
                                    <textarea class="form-control" name="desciptionProduct" rows='2'>{{ $pro->description }}</textarea>
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <input class="form-control" name="priceProduct" value="@if($pro['promotion_price'] == 0){{ number_format($pro['unit_price']) }} @else {{ number_format($pro['promotion_price']) }} @endif đồng" style="background: transparent; border: none;" readonly>
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <img src="public/image/product/{{ $pro->image }}" alt="image" height="50">
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <input class="form-control" name="unitProduct" type="text" value="{{ $pro->unit }}" style="background: transparent; border: none;" readonly>
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <input class="form-control" name="newProduct" type="text" value="{{ $pro->new }}" style="background: transparent; border: none;" readonly>
                                </div>
                            </td>

                            <td class="center">
                                <a href="{{ route('editproduct', $pro->id) }}" title="Chỉnh sửa"><i class="fa fa-pencil fa-lg"></i></a>
                            </td>

                            <td class="center">
                            	<a onclick="return confirm('Bạn có chắc chắn muốn xóa sản phẩm này không?')" href="{{ route('xoaproduct', $pro->id) }}" title="Xóa"><i class="fa fa-trash-o fa-lg"></i></a>
                            </td>
                        </tr>
                        @endforeach
	                </tbody>
	            </table>
                <div>{{ $product->links() }}</div>
            </div><!-- /.col-lg-12 -->
    	</div>
    </div>
</section>
<!-- /.content -->

@endsection