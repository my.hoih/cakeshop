@extends('admin.dashboard')
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Product
        <small>Add</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Product</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Hello admin!</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>
    <!-- /.box -->

    <div class="container-fluid">
    	<div class="row">
    		<!-- Display error -->
	        @if(count($errors) > 0)
	            <div class="alert alert-danger">
		            <ul>
		                @foreach ($errors->all() as $error)
		                    <li>{!! $error !!}</li>
		                @endforeach
		            </ul>
	            </div>
	        @endif
	        <!-- //Display error -->
    		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <form action ="{{ route('themsanpham') }}" method="post" enctype="multipart/form-data">
                	<input type="hidden" name="_token" value="{{ csrf_token() }}">
					@include('flash-message')

                    <div class="form-group">
                        <label> Tên sản phẩm </label>
                        <input type="text" class="form-control" name="txtProName" placeholder="Vui lòng nhập tên sản phẩm" required/>
                    </div>

                    <div class="form-group">
                        <label> ID type </label>
                        <select class="form-control" name="txtIdType" id="txtIdType">
	                        <option value="">Mời bạn chọn:</option>
	                        @foreach($cate as $ca)
	                            <option value="{!! $ca['id']!!}">-- {!! $ca['name'] !!}</option>
	                        @endforeach
	                    </select>
                    </div>

                    <div class="form-group">
                        <label> Mô tả </label>
                        <textarea class="form-control" name="txtProDescription" placeholder="Vui lòng nhập mô tả"  required></textarea>
                    </div>

                    <div class="form-group">
                    	<label> Giá </label>
                        <input class="form-control" name="txtProPrice" type="text" onkeypress='return event.charCode >= 48 && event.charCode <=57' placeholder="Vui lòng nhập giá" required>
                    </div>

                    <div class="form-group">
                    	<label> Giảm giá (Nếu không giảm = 0) </label>
                        <input class="form-control" name="txtProPromotionPrice" type="text" onkeypress='return event.charCode >= 48 && event.charCode <=57' placeholder="Vui lòng nhập giảm giá (Nếu có)" required>
                    </div>

                    <div class="form-group">
                        <label> Hình ảnh </label>
                        <input type="file" name="txtProImage" required>
                    </div>

                    <div class="form-group">
                        <label> Đơn vị </label>
                        <input class="form-control" type="text" name="txtProUnit" placeholder="Vui lòng nhập đơn vị cho sản phẩm" required>
                    </div>

                    <div class="form-group">
                    	<label> Loại hình (Sản phẩm mới là 1) </label>
	                    <input class="form-control" name="txtProNew" type="text" placeholder="Vui lòng nhập loại hình" required>
	                </div>

                    <button type="submit" name="insertProduct" class="btn btn-info"> Thêm </button>
                    <button type="reset" class="btn btn-default"> Thiết lập lại </button>
                </form>
            </div><!-- /.col-lg-6 -->
    	</div>
    </div>
</section>
<!-- /.content -->

@endsection