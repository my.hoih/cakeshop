@foreach($news as $news)
    <tr id="{{$news->id}}">
        <td>{{ $news->id }}</td>
        <td>{{ $news->title }}</td>
        <td>{{ $news->content }}</td>

        <td>
            <a class="btn btn-success" id="edit" data-id="{{ $news->id }}">Edit</a>
        </td>

        <td>
            <a class="btn btn-danger" id="del" data-id="{{ $news->id }}">Delete</a>
        </td>
    </tr>
@endforeach