<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tin tức mới</h4>
            </div>
            <form action="{{ URL::to('admin/store') }}" method="POST" id="frm-insert" >
                <div class="modal-body">

                    <div class="col-4-md">
                        <div class="form-group">
                            <label>Title</label>
                            <input type="text" name="title" class="form-control">
                        </div>
                    </div>

                    <div class="col-4-md">
                        <div class="form-group">
                            <label>Content</label>
                            <input type="text" name="content" class="form-control">
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-success pull-left" value="Save">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>