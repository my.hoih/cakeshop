@extends('admin.dashboard')
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Category
        <small>Edit</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Category</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Hello admin!</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>
    <!-- /.box -->

    <div class="container-fluid">
    	<div class="row">
    		<!-- Display error -->
	        @if(count($errors) > 0)
	            <div class="alert alert-danger">
		            <ul>
		                @foreach ($errors->all() as $error)
		                    <li>{!! $error !!}</li>
		                @endforeach
		            </ul>
	            </div>
	        @endif
	        <!-- //Display error -->
    		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <form action ="{{ route('editcategory',$EditCate->id) }}" method="post" enctype="multipart/form-data">
                	<input type="hidden" name="_token" value="{{ csrf_token() }}">
					@include('flash-message')

                    <div class="form-group">
                        <label> Tên danh mục </label>
                        <input type="text" class="form-control" name="txtCateName" placeholder="Vui lòng nhập tên danh mục sản phẩm" required value="{!! old ('txtname',isset($EditCate)?$EditCate['name']:NULL) !!}" />
                    </div>
                    <div class="form-group">
                        <label> Mô tả </label>
                        <textarea class="form-control" name="txtCateDescription" placeholder="Vui lòng nhập mô tả"  required >{!! old ('txtCateDescription',isset($EditCate)?$EditCate['description']:NULL) !!}</textarea>
                    </div>
                    <div class="form-group">
                        <label> Hình ảnh </label>
                        <input type="file" name="txtimage" value="{!! isset($EditCate)?$EditCate['image']:NULL !!}" required="">
                        <img src="{!! asset('public/image/product/'.$EditCate['image']) !!}" width="100">
                    </div>

                    <button type="submit" name="editCategory" class="btn btn-info"> Chỉnh sửa </button>
                </form>
            </div><!-- /.col-lg-6 -->
    	</div>
    </div>
</section>
<!-- /.content -->

@endsection