@extends('admin.dashboard')
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Bill
        <small>List</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Bill</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Hello admin!</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>
    <!-- /.box -->

    <div class="container-fluid">
    	<div class="row">
            <!-- Display message -->
            @include('flash-message')
            <!-- //Display message -->
    		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
	                <thead>
	                    <tr>
                            <th>ID</th>
	                        <th>Đơn hàng</th>
	                        <th>Thanh toán</th>
                            <th>Note</th>
                            <th>Thông tin khách hàng</th>
                            <th>Thông tin sản phẩm</th>
	                    </tr>
	                </thead>
	                <tbody>
	                	@foreach($view as $views)
                        <tr class="odd gradeX">
                            <td>{{ $views->id }}</td>
                            <td>
                                <div class="form-group">
                                    <span style="float: left;">Tổng tiền: {{ $views->total }} </span>
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <span>{{ $views->payment }}</span>
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <textarea class="form-control">{{ $views->note }}</textarea>
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <span style="float: left;">Mã khách hàng: {{ $views->id_customer }} </span><br>
                                    <span style="float: left;">Tên khách hàng: {{ $views->nameCus }} </span><br>
                                    <span style="float: left;">Email: {{ $views->email }} </span><br>
                                    <span style="float: left;">Địa chỉ: {{ $views->address }} </span><br>
                                    <span style="float: left;">Số điện thoại: {{ $views->phone_number }} </span>
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <span style="float: left;">Mã sản phẩm: {{ $views->id_product }} </span><br>
                                    <span style="float: left;">Tên sản phẩm: {{ $views->name }} </span><br>
                                    <span style="float: left;">Số lượng: {{ $views->quantity }} </span>
                                </div>
                            </td>

                            <td>
                                <a onclick="return confirm('Bạn có chắc chắn muốn xóa không?')" href="{{ route('xoabill',$views->id) }}" title="Xóa"><i class ="fa fa-trash-o fa-lg"></i></a>
                            </td>
                        </tr>
                        @endforeach
	                </tbody>
	            </table>
            </div><!-- /.col-lg-12 -->
    	</div>
    </div>
</section>
<!-- /.content -->

@endsection