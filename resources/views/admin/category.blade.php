@extends('admin.dashboard')
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Category
        <small>List</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Category</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Hello admin!</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>
    <!-- /.box -->

    <div class="container-fluid">
    	<div class="row">
            <!-- Display message -->
            @include('flash-message')
            <!-- //Display message -->
    		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
	                <thead>
	                    <tr>
	                        <th>ID</th>
	                        <th>Tên danh mục sản phẩm</th>
	                        <th>Mô tả</th>
	                        <th>Hình ảnh</th>
	                        <th>Chỉnh sửa</th>
	                        <th>Xóa</th>
	                    </tr>
	                </thead>
	                <tbody>
	                	@foreach($cate as $cates)
                        <tr class="odd gradeX" align="center">
                            <td>{{ $cates->id }}</td>
                            <td>
                                <div class="form-group">
                                    <input class="form-control" name="nameCate" type="text" value="{{ $cates->name }}" style="background: transparent; border: none;" readonly>
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <textarea class="form-control" name="desciptionCate" rows='2'>{{ $cates->description }}</textarea>
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <img src="public/image/product/{{ $cates->image }}" alt="image" height="50">
                                </div>
                            </td>

                            <td class="center">
                                <a href="{{ route('editcategory',$cates->id) }}" title="Chỉnh sửa"><i class="fa fa-pencil fa-lg"></i></a>
                            </td>

                            <td class="center">
                            	<a onclick="return confirm('Bạn có chắc chắn muốn xóa danh mục này không?')" href="{{ route('xoacategory', $cates->id) }}" title="Xóa"><i class="fa fa-trash-o fa-lg"></i></a>
                            </td>
                        </tr>
                        @endforeach
	                </tbody>
	            </table>
            </div><!-- /.col-lg-12 -->
    	</div>
    </div>
</section>
<!-- /.content -->

@endsection