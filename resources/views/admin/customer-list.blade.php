@extends('admin.dashboard')
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Customer
        <small>List</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Customer</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Hello admin!</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>
    <!-- /.box -->

    <div class="container-fluid">
    	<div class="row">
            <!-- Display message -->
            @include('flash-message')
            <!-- //Display message -->
    		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
	                <thead>
	                    <tr>
	                        <th>ID</th>
	                        <th>Tên khách hàng</th>
	                        <th>Giới tính</th>
                            <th>Email</th>
                            <th>Địa chỉ</th>
                            <th>Số điện thoại</th>
	                        <th>Note</th>
	                        <th>Xóa</th>
	                    </tr>
	                </thead>
	                <tbody>
	                	@foreach($customer as $cus)
                        <tr class="odd gradeX" align="center">
                            <td>{{ $cus->id }}</td>
                            <td>
                                <div class="form-group">
                                    <input class="form-control" name="nameCus" type="text" value="{{ $cus->nameCus }}" style="background: transparent; border: none;" readonly>
                                </div>
                            </td>

                            <td>
                                <div class="form-group" style="width: 70px; float: left;">
                                    <input class="form-control" name="genderCus" type="text" value="{{ $cus->gender }}" style="background: transparent; border: none;" readonly>
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <input class="form-control" name="emailUser" type="text" value="{{ $cus->email }}" style="background: transparent; border: none;" readonly>
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <input class="form-control" name="addressCus" type="text" value="{{ $cus->address }}" style="background: transparent; border: none;" readonly>
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <input class="form-control" name="phoneCus" type="text" value="{{ $cus->phone_number }}" style="background: transparent; border: none;" readonly>
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <textarea class="form-control" name="noteCus">{{ $cus->note }}</textarea>
                                </div>
                            </td>

                            <td class="center">
                            	<a onclick="return confirm('Bạn có chắc chắn muốn xóa tài khoản này không?')" href="{{ route('xoacustomer', $cus->id) }}" title="Xóa"><i class="fa fa-trash-o fa-lg"></i></a>
                            </td>
                        </tr>
                        @endforeach
	                </tbody>
	            </table>
            </div><!-- /.col-lg-12 -->
    	</div>
    </div>
</section>
<!-- /.content -->

@endsection