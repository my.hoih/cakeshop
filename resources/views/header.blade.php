<div id="header">
	<div class="header-top">
		<div class="container">
			<div class="pull-left auto-width-left">
				<ul class="top-menu menu-beta l-inline">
					<li><a href=""><i class="fa fa-home"></i> 101B Lê Hữu Trác, Sơn Trà, Đà Nẵng</a></li>
					<li><a href=""><i class="fa fa-phone"></i> 0169 745 0200</a></li>
				</ul>
			</div>
			<div class="pull-right auto-width-right">
				<ul class="top-details menu-beta l-inline">
					@if(Auth::check())
						<li><a href=""><i class="fa fa-user"></i>{{ Auth::user()->name }}</a></li>
						<li><a href="{{ route('logout') }}"><i class="fa fa-sign-out"></i>Đăng xuất</a></li>
					@else
						<li><a href="{{ route('login') }}">Đăng nhập</a></li>
						<li><a href="{{ route('signup') }}">Đăng kí</a></li>
					@endif
				</ul>
			</div>
			<div class="clearfix"></div>
		</div> <!-- .container -->
	</div> <!-- .header-top -->
	<div class="header-body">
		<div class="container beta-relative">
			<div class="pull-left">
				<a href="{{ route('trangchu') }}" id="logo"><img src="public/assets/dest/images/my-logo.png" width="180px" height="100" alt=""></a>
			</div>
			<div class="pull-right beta-components space-left ov">
				<div class="space10">&nbsp;</div>
				<div class="beta-comp">
					<form role="search" method="get" id="searchform" action="{{ route('search') }}">
						<input type="text" value="" name="key" id="s" placeholder="Nhập từ khóa..." />
						<button class="fa fa-search" type="submit" id="searchsubmit"></button>
					</form>
				</div>

				<div class="beta-comp">
					<div class="cart">
						<div class="beta-select"><i class="fa fa-shopping-cart"></i>
							Giỏ hàng (@if(Session::has('cart')){{ Session('cart')->totalQty }} @else Trống @endif)
							<i class="fa fa-chevron-down"></i>
						</div>

						<div class="beta-dropdown cart-body">
							@if(Session::has('cart'))
							@foreach($product_cart as $product)
							<div class="cart-item" id="cart-item{{ $product['item']['id'] }}">
								<a class="cart-item-delete" href="{{ route('xoagiohang',$product['item']['id']) }}"><i class="fa fa-times"></i></a>
								<div class="media">
									<a class="pull-left" href="#"><img src="public/image/product/{{ $product['item']['image'] }}" alt=""></a>
									<div class="media-body">
										<span class="cart-item-title">{{ $product['item']['name'] }}</span>
										<span class="cart-item-amount">{{ $product['qty'] }}*
											<span id="dongia{{ $product['item']['id'] }}"
											value="@if($product['item']['promotion_price']==0) {{ ($product['item']['unit_price']) }} @else {{ ($product['item']['promotion_price']) }} @endif">@if($product['item']['promotion_price']==0){{ number_format($product['item']['unit_price']) }} @else {{ number_format($product['item']['promotion_price']) }} @endif</span>
										</span>
									</div>
								</div>
							</div>
							@endforeach
							<div class="cart-caption">
								<div class="cart-total text-right">Tổng tiền: <span class="cart-total-value">@if(Session::has('cart')){{ number_format($totalPrice) }} @else 0 @endif đồng</span></div>
								<div class="clearfix"></div>

								<div class="center">
									<div class="space10">&nbsp;</div>
									<a href="{{ route('dathang') }}" class="beta-btn primary text-center">Đặt hàng <i class="fa fa-chevron-right"></i></a>
								</div>
							</div>
							@endif
						</div>
					</div> <!-- .cart -->
				</div>
			</div>
			<div class="clearfix"></div>
		</div> <!-- .container -->
	</div> <!-- .header-body -->
	<div class="header-bottom" style="background-color: #0277b8;">
		<div class="container">
			<a class="visible-xs beta-menu-toggle pull-right" href="#"><span class='beta-menu-toggle-text'>Menu</span> <i class="fa fa-bars"></i></a>
			<div class="visible-xs clearfix"></div>
			<nav class="main-menu">
				<ul class="l-inline ov">
					<li><a href="{{ route('trangchu') }}">Trang chủ</a></li>
					<li><a>Sản phẩm</a>
						<ul class="sub-menu">
							@foreach($type_product as $lsp)
							<li>
								<a href="{{ route('loaisanpham',$lsp->id) }}">{{ $lsp->name }}</a>
							</li>
							@endforeach
						</ul>
					</li>
					<li><a href="{{ route('gioithieu') }}">Giới thiệu</a></li>
					<li><a href="{{ route('lienhe') }}">Liên hệ</a></li>
				</ul>
				<div class="clearfix"></div>
			</nav>
		</div> <!-- .container -->
	</div> <!-- .header-bottom -->
</div> <!-- #header -->