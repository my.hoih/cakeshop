<div id="footer" class="color-div">
	<div class="container">
		<div class="row">
			<div class="col-sm-3">
				<div class="widget">
					<h4 class="widget-title">Shop</h4>
					<div><a href="{{ route('trangchu') }}" id="logo"><img src="public/assets/dest/images/my-logo.png" width="180px" height="100" alt="logo"></a></div>
				</div>
			</div>
			<div class="col-sm-2">
				<div class="widget">
					<h4 class="widget-title">Information</h4>
					<div>
						<ul>
							<li><a href="#"><i class="fa fa-chevron-right"></i> Web Design</a></li>
							<li><a href="#"><i class="fa fa-chevron-right"></i> Web development</a></li>
							<li><a href="#"><i class="fa fa-chevron-right"></i> Marketing</a></li>
							<li><a href="#"><i class="fa fa-chevron-right"></i> Tips</a></li>
							<li><a href="#"><i class="fa fa-chevron-right"></i> Resources</a></li>
							<li><a href="#"><i class="fa fa-chevron-right"></i> Illustrations</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="col-sm-10">
					<div class="widget">
						<h4 class="widget-title">Contact Us</h4>
						<div>
							<div class="contact-info">
								<i class="fa fa-map-marker"></i>
								<p>101B Lê Hữu Trác-Sơn Trà-Đà Nẵng Phone: +84 169 745 0200</p>
								<p>Nemo enim ipsam voluptatem quia voluptas sit asnatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="widget">
					<h4 class="widget-title">Newsletter Subscribe</h4>
					<form action="#" method="post">
						<input type="email" name="your_email">
						<button class="pull-right" type="submit">Subscribe <i class="fa fa-chevron-right"></i></button>
					</form>
				</div>
			</div>
		</div> <!-- .row -->
	</div> <!-- .container -->
</div> <!-- #footer -->
<div class="copyright">
	<div class="container">
		<p class="pull-left">Privacy policy. (&copy;) 2018</p>
		<p class="pull-right pay-options">
			<a href="#"><img src="public/assets/dest/images/pay/MasterCard.png" alt="" height="20" /></a>
			<a href="#"><img src="public/assets/dest/images/pay/Visa.jpg" alt="" height="20" /></a>
			<a href="#"><img src="public/assets/dest/images/pay/Paypal.jpg" alt="" height="20" /></a>
		</p>
		<div class="clearfix"></div>
	</div> <!-- .container -->
</div> <!-- .copyright -->