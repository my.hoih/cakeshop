@extends('master')
@section('content')

<div class="inner-header">
	<div class="container">
		<div class="pull-left">
			<h6 class="inner-title">Chi Tiết Sản Phẩm</h6>
		</div>
		<div class="pull-right">
			<div class="beta-breadcrumb font-large">
				<a href="{{ route('trangchu') }}">Trang Chủ</a> / <span>Chi Tiết Sản Phẩm</span>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>

<div class="container">
	<div id="content">
		<div class="row">
			<div class="col-sm-9">
				<div class="row">
					<div class="col-sm-4">
						<img src="public/image/product/{{ $sanpham->image }}" alt="{{ $sanpham->name }}">
					</div>
					<div class="col-sm-8">
						<div class="single-item-body">
							<p class="single-item-title"><h3>{{ $sanpham->name }}</h3></p>
							<p class="single-item-price">
								@if ($sanpham->promotion_price > 0)
								<span class="flash-del">{{ number_format($sanpham->unit_price) }} đ</span>
								<span class="flash-sale">{{ number_format($sanpham->promotion_price) }} đ</span>
								@else
								<span class="flash-sale">{{ number_format($sanpham->unit_price) }} đ</span>
								@endif
							</p>
						</div>

						<div class="clearfix"></div>
						<div class="space20">&nbsp;</div>

						<div class="single-item-desc">
							<p>{{ $sanpham->description }}</p>
						</div>
						<div class="space20">&nbsp;</div>

						<p>Số lượng:</p>
						<div class="single-item-options">
							<select class="wc-select" name="color">
								<option>Số lượng</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
							</select>
							<a class="add-to-cart" href="{{ route('themgiohang',$sanpham->id) }}"><i class="fa fa-shopping-cart"></i></a>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>

				<div class="space40">&nbsp;</div>
				<div class="woocommerce-tabs">
					<ul class="tabs">
						<li><a href="#tab-description">Mô tả</a></li>
						<li><a href="#tab-reviews">Reviews (0)</a></li>
					</ul>

					<div class="panel" id="tab-description">
						<p>{{ $sanpham->description }}</p><br>
					</div>
					<div class="panel" id="tab-reviews">
						<p>No Reviews</p>
					</div>
				</div>
				<div class="space50">&nbsp;</div>
				<div class="beta-products-list">
					<h4>Related Products - Sản phẩm liên quan</h4>
					<p class="pull-left">Tìm thấy: {{count($sp_tuongtu)}} sản phẩm</p>
					<div class="clearfix"></div>
					<div class="row">
						@foreach($sp_tuongtu as $sptt)
						<div class="col-sm-4">
							<div class="single-item">
								<div class="single-item-header">
									<a href="{{ route('chitietsanpham',$sptt->id) }}"><img src="public/image/product/{{ $sptt->image }}" alt="{{ $sptt->name }}" height="200"></a>
								</div>
								<div class="single-item-body">
									<p class="single-item-title">{{ $sptt->name }}</p>
									<p class="single-item-price">
										@if ($sptt->promotion_price > 0)
										<span class="flash-del">{{ number_format($sptt->unit_price) }} đ</span>
										<span class="flash-sale">{{ number_format($sptt->promotion_price) }} đ</span>
										@else
										<span class="flash-sale">{{ number_format($sptt->unit_price) }} đ</span>
										@endif
									</p>
								</div>
								<div class="single-item-caption">
									<a class="add-to-cart pull-left" href="{{ route('themgiohang',$sptt->id) }}"><i class="fa fa-shopping-cart"></i></a>
									<a class="beta-btn primary" href="{{ route('chitietsanpham',$sptt->id) }}">Details <i class="fa fa-chevron-right"></i></a>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
						@endforeach
					</div>
					<div>{{ $sp_tuongtu->links() }}</div>
				</div> <!-- .beta-products-list -->
			</div>
			<div class="col-sm-3 aside">
				<div class="widget">
					<h3 class="widget-title">Best Sellers</h3>
					<div class="widget-body">
						<div class="beta-sales beta-lists">
							<div class="media beta-sales-item">
								<a class="pull-left" href="#"><img src="public/image/product/1234.jpg" alt=""></a>
								<div class="media-body">
									Bánh
									<span class="beta-sales-price">30,000đ</span>
								</div>
							</div>
						</div>
					</div>
				</div> <!-- best sellers widget -->
				<div class="widget">
					<h3 class="widget-title">New Products</h3>
					<div class="widget-body">
						<div class="beta-sales beta-lists">
							@foreach($sp_moi as $spm)
							<div class="media beta-sales-item">
								<a class="pull-left" href="{{ route('chitietsanpham',$spm->id) }}}"><img src="public/image/product/{{ $spm->image }}" alt="{{ $spm->name }}"></a>
								<div class="media-body">
									{{ $spm->name }}
									@if ($spm->promotion_price > 0)
										<span class="flash-del">{{ number_format($spm->unit_price) }} đ</span>
										<span class="beta-sales-price">{{ number_format($spm->promotion_price) }} đ</span>
									@else
										<span class="beta-sales-price">{{ number_format($spm->unit_price) }} đ</span>
									@endif
								</div>
							</div>
							@endforeach
						</div>
					</div>
					<div>{{ $sp_moi->links() }}</div>
				</div> <!-- best sellers widget -->
			</div>
		</div>
	</div> <!-- #content -->
</div> <!-- .container -->

@endsection