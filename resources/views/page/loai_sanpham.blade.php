@extends('master')
@section('content')

<div class="inner-header">
	<div class="container">
		<div class="pull-left">
			<h6 class="inner-title">Loại Sản phẩm</h6>
		</div>
		<div class="pull-right">
			<div class="beta-breadcrumb font-large">
				<a href="{{ route('trangchu') }}">Trang Chủ</a> / <span>Loại Sản phẩm</span>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div class="container">
	<div id="content" class="space-top-none">
		<div class="main-content">
			<div class="space60">&nbsp;</div>
			<div class="row">
				<div class="col-sm-3">
					<ul class="aside-menu">
						@foreach($loai_menuleft as $lsp)
						<li>
							<a href="{{ route('loaisanpham',$lsp->id) }}">{{ $lsp->name }}</a>
						</li>
						@endforeach
					</ul>
				</div>
				<div class="col-sm-9">
					<div class="beta-products-list">
						<!-- <h4>Sản Phẩm Mới</h4> -->
						<h4>
							<a href="{{ route('loaisanpham',$loai_sp->id) }}">{{ $loai_sp->name }}</a>
						</h4>
						<div class="beta-products-details">
							<p class="pull-left">Tìm thấy: {{count($sp_tuongtu)}} sản phẩm</p>
							<div class="clearfix"></div>
						</div>

						<div class="row">
							@foreach($sp_tuongtu as $sptt)
							<div class="col-sm-4">
								<div class="single-item">
									<div class="single-item-header">
										<a href="{{ route('chitietsanpham',$sptt->id) }}"><img src="public/image/product/{{ $sptt->image }}" alt="{{ $sptt->name }}" height="200"></a>
									</div>
									<div class="single-item-body">
										<p class="single-item-title">{{ $sptt->name }}</p>
										<p class="single-item-price">
											@if ($sptt->promotion_price > 0)
											<span class="flash-del">{{ number_format($sptt->unit_price) }} đ</span>
											<span class="flash-sale">{{ number_format($sptt->promotion_price) }} đ</span>
											@else
											<span class="flash-sale">{{ number_format($sptt->unit_price) }} đ</span>
											@endif
										</p>
									</div>
									<div class="single-item-caption">
										<a class="add-to-cart pull-left" href="{{ route('themgiohang',$sptt->id) }}"><i class="fa fa-shopping-cart"></i></a>
										<a class="beta-btn primary" href="{{ route('chitietsanpham',$sptt->id) }}">Details <i class="fa fa-chevron-right"></i></a>
										<div class="clearfix"></div>
									</div>
								</div>
							</div>
							@endforeach
						</div>
						<div>{{ $sp_tuongtu->links() }}</div>
					</div> <!-- .beta-products-list -->

					<div class="space50">&nbsp;</div>

					<div class="beta-products-list">
					<h4>Sản Phẩm Mới</h4>
					
					<div class="beta-products-details">
							<p class="pull-left">Tìm thấy: {{count($new_product)}} sản phẩm</p>
							<div class="clearfix"></div>
						</div>

						<div class="row">
							@foreach($new_product as $new)
							<div class="col-sm-3">
								<div class="single-item">
									@if($new->promotion_price != 0)
										<div class="ribbon-wrapper"><div class="ribbon sale">Khuyến Mãi</div></div>
									@endif
									<div class="single-item-header">
										<a href="{{ route('chitietsanpham',$new->id) }}"><img src="public/image/product/{{ $new->image }}" alt="{{ $new->name }}" height="200"></a>
									</div>
									<div class="single-item-body">
										<p class="single-item-title">{{ $new->name }}</p>
										<p class="single-item-price">
											@if ($new->promotion_price > 0)
												<span class="flash-del">{{ number_format($new->unit_price) }} đ</span>
												<span class="flash-sale">{{ number_format($new->promotion_price) }} đ</span>
											@else
												<span class="flash-sale">{{ number_format($new->unit_price) }} đ</span>
											@endif
										</p>
									</div>
									<div class="single-item-caption">
										<a class="add-to-cart pull-left" href="{{ route('themgiohang',$new->id) }}"><i class="fa fa-shopping-cart"></i></a>
										<a class="beta-btn primary" href="{{ route('chitietsanpham',$new->id) }}">Details <i class="fa fa-chevron-right"></i></a>
										<div class="clearfix"></div>
									</div>
								</div>
							</div><!-- /.col -->
							@endforeach
						</div>
						<!-- Phân trang -->
						<div>{{ $new_product->links() }}</div>
					</div> <!-- .beta-products-list -->
				</div>
			</div> <!-- end section with sidebar and main content -->


		</div> <!-- .main-content -->
	</div> <!-- #content -->
</div> <!-- .container -->

@endsection