@extends('master')
@section('content')
	<div class="inner-header">
		<div class="container">
			<div class="pull-left">
				<h6 class="inner-title">Đăng kí</h6>
			</div>
			<div class="pull-right">
				<div class="beta-breadcrumb font-large">
					<a href="{{ route('trangchu') }}">Trang Chủ</a> / <span>Đăng kí</span>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	
	<div class="container">
		<div id="content">
			<!-- Display error -->
	        @if(count($errors) > 0)
	            <div class="alert alert-danger">
		            <ul>
		                @foreach ($errors->all() as $error)
		                    <li>{!! $error !!}</li>
		                @endforeach
		            </ul>
	            </div>
	        @endif
	        <!-- //Display error -->
			<form action="{{ route('signup') }}" method="post" class="beta-form-checkout">
				<input type="hidden" name="_token" value="{{csrf_token()}}">
				@include('flash-message')

				<div class="row">
					<div class="col-sm-3"></div>
					<div class="col-sm-6">
						<h4>Đăng kí</h4>
						<div class="space20">&nbsp;</div>
						
						<div class="form-block">
							<label for="email">Email address*</label>
							<input type="email" name="email" placeholder="expample@gmail.com" required>
						</div>

						<div class="form-block">
							<label for="your_last_name">Fullname*</label>
							<input type="text" name="fullname" placeholder="Họ tên" required>
						</div>

						<div class="form-block">
							<label>Gender*</label>
							<input id="gender" type="radio" class="input-radio" name="gender" value="Nam" checked="checked" style="width: 10%"><span style="margin-right: 10%">Nam</span>
							<input id="gender" type="radio" class="input-radio" name="gender" value="Nữ" style="width: 10%"><span>Nữ</span>
						</div>

						<div class="form-block">
							<label for="address">Address*</label>
							<input type="text" name="address" placeholder="Địa chỉ" required>
						</div>

						<div class="form-block">
							<label for="phone">Phone*</label>
							<input type="text" onkeypress='return event.charCode >= 48 && event.charCode <=57' name="phone" placeholder="Số điện thoại" required>
						</div>

						<div class="form-block">
							<label for="password">Password*</label>
							<input type="password" name="password" required>
						</div>

						<div class="form-block">
							<label for="re_password">Re password*</label>
							<input type="password" name="re_password" required>
						</div>
						
						<div class="form-block">
							<button type="submit" class="btn btn-primary">Register</button>
						</div>
					</div>
					<div class="col-sm-3"></div>
				</div>
			</form>
		</div> <!-- #content -->
	</div> <!-- .container -->

	<!--customjs-->
	<script type="text/javascript">
	    $(function() {
	        // this will get the full URL at the address bar
	        var url = window.location.href;

	        // passes on every "a" tag
	        $(".main-menu a").each(function() {
	            // checks if its the same on the address bar
	            if (url == (this.href)) {
	                $(this).closest("li").addClass("active");
					$(this).parents('li').addClass('parent-active');
	            }
	        });
	    });   
	</script>

	<script>
	 	jQuery(document).ready(function($) {
            'use strict';
			// color box

			//color
	      	jQuery('#style-selector').animate({
		      	left: '-213px'
		    });

    		jQuery('#style-selector a.close').click(function(e){
      			e.preventDefault();
      			var div = jQuery('#style-selector');
      			if (div.css('left') === '-213px')
      			{
        			jQuery('#style-selector').animate({
          				left: '0'
        			});
        			jQuery(this).removeClass('icon-angle-left');
        			jQuery(this).addClass('icon-angle-right');
      			} else {
    				jQuery('#style-selector').animate({
          				left: '-213px'
        			});
        			jQuery(this).removeClass('icon-angle-right');
					jQuery(this).addClass('icon-angle-left');
      			}
    		});
		});
	</script>

@endsection