<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Laravel - Hôih My</title>
	<base href="{{asset('')}}"></base>
	<link href='http://fonts.googleapis.com/css?family=Dosis:300,400' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" href="public/assets/dest/css/font-awesome.min.css">
	<link rel="stylesheet" href="public/assets/dest/vendors/colorbox/example3/colorbox.css">
	<link rel="stylesheet" href="public/assets/dest/rs-plugin/css/settings.css">
	<link rel="stylesheet" href="public/assets/dest/rs-plugin/css/responsive.css">
	<link rel="stylesheet" title="style" href="public/assets/dest/css/style.css">
	<link rel="stylesheet" href="public/assets/dest/css/animate.css">
	<link rel="stylesheet" title="style" href="public/assets/dest/css/huong-style.css">
</head>
<body>
	
	@include('header')
	
	<div class="rev-slider">
		@yield('content')
	</div>

	@include('footer')

	@include('script')
	
</body>
</html>
