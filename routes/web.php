<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
// ----------------------------------------------------------------
// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
// ----------------------------------------------------------------
Route::group(['prefix' => 'hoihmy/'], function() {
	Route::get('trang_chu', [
		'as' 	=> 'trangchu',
		'uses' 	=> 'PageController@getIndex'
	]);

	Route::get('loai_sanpham/{type}', [
		'as' 	=> 'loaisanpham',
		'uses' 	=> 'PageController@getLoaiSp'
	]);

	Route::get('chitiet_sanpham/{id}', [
		'as' 	=> 'chitietsanpham', //tên định danh
		'uses' 	=> 'PageController@getChitietSp'
	]);

	Route::get('lien_he', [
		'as' 	=> 'lienhe',
		'uses' 	=> 'PageController@getLienhe'
	]);

	Route::get('gioi_thieu', [
		'as' 	=> 'gioithieu',
		'uses' 	=> 'PageController@getGioithieu'
	]);

	Route::get('add-to-cart/{id}', [
		'as' 	=> 'themgiohang',
		'uses' 	=> 'PageController@getAddtoCart'
	]);

	Route::get('del-cart/{id}', [
		'as' 	=> 'xoagiohang',
		'uses' 	=> 'PageController@getDelItemCart'
	]);

	Route::get('dat-hang', [
		'as' 	=> 'dathang',
		'uses' 	=> 'PageController@getCheckout'
	]);

	Route::post('dat-hang', [
		'as'	=> 'dathang',
		'uses'	=> 'PageController@postCheckout'
	]);

	Route::get('dang-nhap', [
		'as'	=> 'login',
		'uses'	=> 'PageController@getLogin'
	]);

	Route::post('dang-nhap', [
		'as'	=> 'login',
		'uses'	=> 'PageController@postLogin'
	]);

	Route::get('dang-ky', [
		'as'	=> 'signup',
		'uses'	=> 'PageController@getSignup'
	]);

	Route::post('dang-ky', [
		'as'	=> 'signup',
		'uses'	=> 'PageController@postSignup'
	]);

	Route::get('dang-xuat',[
		'as'	=> 'logout',
		'uses'	=> 'PageController@postLogout'
	]);

	Route::get('search',[
		'as'	=> 'search',
		'uses'	=> 'PageController@getSearch'
	]);
});

// ------------------------------------------------
// *** admin page ***
Route::get('dashboard', function() {
	return view('admin.dashboard');
});

Route::get('login', function() {
	return view('admin.login');
});

// Danh mục sản phẩm
Route::get('category',[
	'as'	=> 'danhmucsp',
	'uses'	=> 'PageController@getCategory'
]);

// Thêm category
Route::get('add-category',[
	'as'	=> 'themcategory',
	'uses'	=> 'PageController@getAddCate'
]);

Route::post('add-category',[
	'as'	=> 'themcategory',
	'uses'	=> 'PageController@postAddCate'
]);

// Sửa category
Route::get('edit-category/{id}',[
	'as'	=> 'editcategory',
	'uses'	=> 'PageController@getEditCate'
]);

Route::post('edit-category/{id}',[
	'as'	=> 'editcategory',
	'uses'	=> 'PageController@postEditCate'
]);

// Xóa category
Route::get('del-category/{id}', [
	'as'    => 'xoacategory',
	'uses'	=> 'PageController@getDelCate'
]);

// Sản phẩm
Route::get('product',[
	'as'	=> 'sanpham',
	'uses'	=> 'PageController@getProduct'
]);

// Thêm sản phẩm
Route::get('add-product',[
	'as'	=> 'themsanpham',
	'uses'	=> 'PageController@getAddPro'
]);

Route::post('add-product', [
	'as'	=> 'themsanpham',
	'uses'	=> 'PageController@postAddPro'
]);

// Sửa sản phẩm
Route::get('edit-product/{id}',[
	'as'	=> 'editproduct',
	'uses'	=> 'PageController@getEditPro'
]);

Route::post('edit-product/{id}',[
	'as'	=> 'editproduct',
	'uses'	=> 'PageController@postEditPro'
]);

// Xóa sản phẩm
Route::get('del-product/{id}', [
	'as'    => 'xoaproduct',
	'uses'	=> 'PageController@getDelPro'
]);

// Danh sách có tài khoản
Route::get('user', [
	'as'    => 'user',
	'uses'	=> 'PageController@getUser'
]);

// Danh sách khách hàng
Route::get('customer', [
	'as'    => 'customer',
	'uses'	=> 'PageController@getCustomer'
]);

// Xóa tài khoản khách hàng
Route::get('del-customer/{id}', [
	'as'    => 'xoacustomer',
	'uses'	=> 'PageController@getDelCus'
]);

// Đơn hàng
Route::get('bill', [
	'as'    => 'bill',
	'uses'	=> 'PageController@getBill'
]);

Route::get('del-bill/{id}', [
	'as'    => 'xoabill',
	'uses'	=> 'PageController@getDelBill'
]);

// ----------------------------------------------------------------------
// *** Use ajax + jQuery: ***
Route::get('/news' ,'NewsController@index')->name('index');

Route::get('/admin/read-data', 'NewsController@readData');

Route::post('/admin/store', 'NewsController@store');

Route::post('/admin/destroy', 'NewsController@destroy');

Route::get('/admin/edit', 'NewsController@edit');

Route::post('/admin/update', 'NewsController@update');